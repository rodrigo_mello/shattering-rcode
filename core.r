library(gmp)
require(fastcluster)

find.class <- function(labels, vec, p) {
	if (p < 0) {
		id = -p
		return (labels[id])
	} else {
		return (vec[p])
	}
}

shrink.space <- function(dataset, 
			 data.dimensionality,
			 hclust.maxsize=10000, 
			 method="complete", 
			 correlations=NULL, 
			 labels=NULL) {

	#classId = ncol(dataset)
	#labels = as.numeric(dataset[,classId])
	labels = labels

	euclidean = correlations
	if (is.null(correlations)) {
		# TODO: we must compute the clustering several times
		euclidean = parallelDist::parDist(dataset)
	} else {
		euclidean = as.dist(correlations)
	}

	hmodel = fastcluster::hclust(euclidean, method)
	vec = rep(0, nrow(hmodel$merge))

	for (i in 1:nrow(hmodel$merge)) {
		connection = hmodel$merge[i,]

		c1 = find.class(labels, vec, connection[1])
		c2 = find.class(labels, vec, connection[2])

		if (c1 == c2) {
			vec[i] = c1
		} else {
			vec[i] = -1
		}
	}

	result = cbind(hmodel$merge, vec)

	# setting classes
	classes = rep(NA, length(labels))
	uqn = unique(labels)
	for (i in 1:length(uqn)) {
		rid = which(vec == uqn[i])
		elems = as.numeric(hmodel$merge[rid,])
		id = which(elems < 0)
		points = -1 * elems[id]
		classes[points] = uqn[i]
	}

	model = list()
	model$data.dimensionality = data.dimensionality
	model$result = result
	# Vector with the class id or -1 in case of no assignment
	# given this last id means the point will be maintained
	# in the shrinked space to proceed with the hyperplane analysis
	model$vec = vec
	# Hierarchical model
	model$hmodel = hmodel
	# point classes
	model$classes = classes
	# New number of data points in the space

	model$npoints = length(unique(labels)) + sum(is.na(classes))

	return (model)
}

shrink.samples <- function(dataset, labels,
			   data.dimensionality,
			   samp.start=5, 
			   samp.end=50,
			   by=5,
			   hclust.maxsize=10000,
			   method="complete",
			   plot=F,
			   correlations=NULL) {

	if (samp.start < 5 || samp.end < 0) {
		return ("samp.start and samp.end must be >= 5")
	}

	if (samp.start >= samp.end) {
		return ("samp.start must be smaller than samp.end.")
	}

	if (is.null(correlations) && nrow(dataset) < samp.end) {
		return ("Dataset contains less the samp.end rows.")
	}

	res = NULL
	for (i in seq(samp.start, samp.end, by=by)) {
		cat("Sample size ", i, " out of ", samp.end, "\n")
		r = NULL
		if (is.null(correlations)) {
			r = shrink.space(dataset[1:i,], 
					 data.dimensionality=data.dimensionality,
					 hclust.maxsize=hclust.maxsize, 
					 method=method, 
					 labels=labels)
		} else {
			r = shrink.space(dataset=NULL, 
					 data.dimensionality=data.dimensionality,
					 correlations=correlations[1:i,1:i], 
					 hclust.maxsize=hclust.maxsize, 
					 method=method, 
					 labels=labels)
		}
		res = rbind(res, cbind(i, r$npoints))

		if (plot) plot(res)
	}

	ret = NULL
	ret$data.dimensionality = data.dimensionality
	ret$npoints.per.sample = res

	return (ret)
}

number.hyperplanes <- function(shrinked.sample) {

	nhyperplanes = shrinked.sample$data.dimensionality * 
		shrinked.sample$npoints.per.sample[,2]^(2/(shrinked.sample$data.dimensionality+1))

	max.nhyperplanes = rep(0, length(nhyperplanes))

	max = ceiling(nhyperplanes[length(nhyperplanes)])
	for (i in length(nhyperplanes):1) {
		if (ceiling(nhyperplanes[i]) < max) { 
			max = ceiling(nhyperplanes[i]) 
		}
		max.nhyperplanes[i] = max
	}

	shrinked.sample$nhyperplanes = nhyperplanes
	shrinked.sample$max.nhyperplanes = max.nhyperplanes

	return (shrinked.sample)
}

compute.combinatorics <- function(regions, nclasses) {

	if (nclasses > 1) {
		sum = 0 # due to combn(regions, regions)
		for (i in 1:regions) {
			if (regions > i) {
				sum = sum + choose(regions, i) * compute.combinatorics(regions-i, nclasses-1)
			}
		}
		return (sum)
	} else {
		return (1)
	}
}

shattering <- function(shrinked.sample, nclasses=2) {

	M = shrinked.sample$max.nhyperplanes
	shattering.value = c()
	for (i in 1:length(M)) {
		cat(i, " out of ", length(M), "\n")
		shattering.value = c(shattering.value, compute.combinatorics(2^M[i], nclasses))
	}

	shrinked.sample$shattering.function = cbind(shrinked.sample$npoints, shattering.value)

	return (shrinked.sample)
}

compute.shattering <- function(dataset, 
			       labels, 
			       data.dimensionality,
			       samp.start=5, 
			       samp.end=50, by=5, 
			       hclust.maxsize=10000,
			       method="complete", 
			       shat=FALSE, 
			       partial.plot=F,
			       correlations=NULL) {
	rowid=0
	if (is.null(correlations)) {
		rowid = sample(1:nrow(dataset))
		rdataset = dataset[rowid,]
		labels = labels[rowid]
		sked1 = shrink.samples(rdataset, labels, 
			       	       data.dimensionality,
				       samp.start, samp.end, 
				       by, hclust.maxsize, 
				       method, partial.plot)
	} else {
		rowid = sample(1:nrow(correlations))
		rdataset = correlations[rowid,rowid]
		sked1 = shrink.samples(dataset=NULL,
				       data.dimensionality=data.dimensionality,
				       correlations=rdataset, 
				       labels=labels,
				       samp.start=samp.start, 
				       samp.end=samp.end, 
				       by=by, 
				       hclust.maxsize=hclust.maxsize, 
				       method=method, 
				       plot=partial.plot)
	}
	sked2 = number.hyperplanes(sked1)

	if (shat) {
		id = ncol(rdataset)
		sked3 = shattering(sked2, nclasses=length(unique(rdataset[,id])))
		return (sked3)
	} else {
		return (sked2)
	}
}

