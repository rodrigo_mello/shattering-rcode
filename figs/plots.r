###################################################################################
# Gausssians 1
###################################################################################
load("../data/gaussians1_mean0_sd1__mean10_sd1.RData")
pdf("gaussians1.pdf")
plot(gaussians1$D, pch=gaussians1$D[,3], xlab="Attribute 1", ylab="Attribute 2")
dev.off()

#pdf("gaussians1-npoints.pdf")
#plot(gaussians1$npoints.per.sample, xlab="Sample size", ylab="Shrinked sample size")
#dev.off()
###################################################################################

###################################################################################
# Gausssians 2
###################################################################################
load("../data/gaussians2_mean0_sd1__mean2_sd1.RData")
pdf("gaussians2.pdf")
plot(gaussians2$D, pch=gaussians2$D[,3], xlab="Attribute 1", ylab="Attribute 2")
dev.off()

colnames(gaussians2$npoints.per.sample)=c("n", "nshrinked")
model = lm(nshrinked ~ n, data=as.data.frame(gaussians2$npoints.per.sample))
# Usar resultados do modelo para análise

# plotting the shrinking estimation
id=nrow(gaussians2$npoints.per.sample)
n=1:gaussians2$npoints.per.sample[id,1]
nshrinked = 0.10884*n+3.47895
pdf("gaussians2-nshrinked.pdf")
plot(nshrinked, xlab="Sample size", ylab="Estimation of shrinked samples", t="l")
points(gaussians2$npoints.per.sample, pch=2)
dev.off()

# plotting the hyperplane estimation
nhyperplane.estimation = gaussians2$data.dimensionality * nshrinked^(2/(gaussians2$data.dimensionality+1))
pdf("gaussians2-nhyperplanes-estimation.pdf")
data = cbind(gaussians2$npoints.per.sample[,1], gaussians2$max.nhyperplanes)
plot(data, xlab="Sample size", ylab="Number of hyperplanes")
lines(nhyperplane.estimation, xlab="Sample size", ylab="Estimation of hyperplanes")
dev.off()
###################################################################################

###################################################################################
# Boston housing
###################################################################################
load("../data/boston.results.RData")

colnames(boston.results$npoints.per.sample)=c("n", "nshrinked")
model = lm(nshrinked ~ n, data=as.data.frame(boston.results$npoints.per.sample))

id=nrow(boston.results$npoints.per.sample)
n=1:boston.results$npoints.per.sample[id,1]
nshrinked = 0.532756*n+12.943824
pdf("boston.results-nshrinked.pdf")
plot(nshrinked, xlab="Sample size", ylab="Estimation of shrinked samples", t="l")
points(boston.results$npoints.per.sample, pch=2, cex=0.3)
dev.off()

nhyperplane.estimation = boston.results$data.dimensionality * nshrinked^(2/(boston.results$data.dimensionality+1))
pdf("boston.results-nhyperplanes-estimation.pdf")
data = cbind(boston.results$npoints.per.sample[,1], boston.results$max.nhyperplanes)
plot(data, xlab="Sample size", ylab="Number of hyperplanes", t="p", cex=0.1)
lines(nhyperplane.estimation, xlab="Sample size", ylab="Estimation of hyperplanes")
dev.off()
###################################################################################

###################################################################################
# MNIST for 3x3, 5x5, 7x7, 9x9
###################################################################################
load("../data/mnist.results.RData")

colnames(mnist[[3]]$npoints.per.sample)=c("n", "nshrinked")
model3 = lm(nshrinked ~ n, data=as.data.frame(mnist[[3]]$npoints.per.sample))
colnames(mnist[[5]]$npoints.per.sample)=c("n", "nshrinked")
model5 = lm(nshrinked ~ n, data=as.data.frame(mnist[[5]]$npoints.per.sample))
colnames(mnist[[7]]$npoints.per.sample)=c("n", "nshrinked")
model7 = lm(nshrinked ~ n, data=as.data.frame(mnist[[7]]$npoints.per.sample))
colnames(mnist[[9]]$npoints.per.sample)=c("n", "nshrinked")
model9 = lm(nshrinked ~ n, data=as.data.frame(mnist[[9]]$npoints.per.sample))

id=nrow(mnist[[3]]$npoints.per.sample)
n=1:mnist[[3]]$npoints.per.sample[id,1]
nshrinked3 = 0.8889*n + 226.4631

id=nrow(mnist[[5]]$npoints.per.sample)
n=1:mnist[[5]]$npoints.per.sample[id,1]
nshrinked5 = 0.8498*n + 329.7998

id=nrow(mnist[[7]]$npoints.per.sample)
n=1:mnist[[7]]$npoints.per.sample[id,1]
nshrinked7 = 0.791*n + 675.453

id=nrow(mnist[[9]]$npoints.per.sample)
n=1:mnist[[9]]$npoints.per.sample[id,1]
nshrinked9 = 0.7324*n + 863.5583

pdf("mnist-nshrinked.pdf")
#plot(nshrinked3, xlab="Sample size", ylab="Estimation of shrinked samples", t="l")
plot(mnist[[3]]$npoints.per.sample, pch=2, cex=0.8, xlab="Sample size", ylab="Estimation of shrinked samples")
#lines(nshrinked5)
points(mnist[[5]]$npoints.per.sample, pch=3, cex=0.8)
#lines(nshrinked7)
points(mnist[[7]]$npoints.per.sample, pch=4, cex=0.8)
#lines(nshrinked9)
points(mnist[[9]]$npoints.per.sample, pch=5, cex=0.8)
dev.off()

nhyperplane.estimation3 = mnist[[3]]$data.dimensionality * nshrinked3^(2/(mnist[[3]]$data.dimensionality+1))
nhyperplane.estimation5 = mnist[[5]]$data.dimensionality * nshrinked5^(2/(mnist[[5]]$data.dimensionality+1))
nhyperplane.estimation7 = mnist[[7]]$data.dimensionality * nshrinked7^(2/(mnist[[7]]$data.dimensionality+1))
nhyperplane.estimation9 = mnist[[9]]$data.dimensionality * nshrinked9^(2/(mnist[[9]]$data.dimensionality+1))

pdf("mnist-nhyperplanes-estimation.pdf")
data3 = cbind(mnist[[3]]$npoints.per.sample[,1], mnist[[3]]$max.nhyperplanes)
data5 = cbind(mnist[[5]]$npoints.per.sample[,1], mnist[[5]]$max.nhyperplanes)
data7 = cbind(mnist[[7]]$npoints.per.sample[,1], mnist[[7]]$max.nhyperplanes)
data9 = cbind(mnist[[9]]$npoints.per.sample[,1], mnist[[9]]$max.nhyperplanes)
plot(data3, xlab="Sample size", ylab="Number of hyperplanes", t="p", pch=2, cex=1, ylim=c(30,107))
#lines(nhyperplane.estimation3, xlab="Sample size", ylab="Estimation of hyperplanes")
points(data5, pch=3, cex=1)
#lines(nhyperplane.estimation5)
points(data7, pch=4, cex=1)
#lines(nhyperplane.estimation7)
points(data9, pch=5, cex=1)
#lines(nhyperplane.estimation9)
dev.off()
###################################################################################

###################################################################################
# CIFAR-10 for 3x3, 5x5, 7x7, 9x9
###################################################################################
load("../data/cifar10.results.RData")

colnames(cifar10[[3]]$npoints.per.sample)=c("n", "nshrinked")
model3 = lm(nshrinked ~ n, data=as.data.frame(cifar10[[3]]$npoints.per.sample))
colnames(cifar10[[5]]$npoints.per.sample)=c("n", "nshrinked")
model5 = lm(nshrinked ~ n, data=as.data.frame(cifar10[[5]]$npoints.per.sample))
colnames(cifar10[[7]]$npoints.per.sample)=c("n", "nshrinked")
model7 = lm(nshrinked ~ n, data=as.data.frame(cifar10[[7]]$npoints.per.sample))
colnames(cifar10[[9]]$npoints.per.sample)=c("n", "nshrinked")
model9 = lm(nshrinked ~ n, data=as.data.frame(cifar10[[9]]$npoints.per.sample))

id=nrow(cifar10[[3]]$npoints.per.sample)
n=1:cifar10[[3]]$npoints.per.sample[id,1]
nshrinked3 = 0.677*n + 1960.508

id=nrow(cifar10[[5]]$npoints.per.sample)
n=1:cifar10[[5]]$npoints.per.sample[id,1]
nshrinked5 = 0.6139*n + 2837.0457

id=nrow(cifar10[[7]]$npoints.per.sample)
n=1:cifar10[[7]]$npoints.per.sample[id,1]
nshrinked7 = 0.5561*n + 3873.9568

id=nrow(cifar10[[9]]$npoints.per.sample)
n=1:cifar10[[9]]$npoints.per.sample[id,1]
nshrinked9 = 0.5108*n + 4591.5425

pdf("cifar10-nshrinked.pdf")
#plot(nshrinked3, xlab="Sample size", ylab="Estimation of shrinked samples", t="l")
plot(cifar10[[3]]$npoints.per.sample, pch=2, cex=0.8, xlab="Sample size", ylab="Estimation of shrinked samples")
#lines(nshrinked5)
points(cifar10[[5]]$npoints.per.sample, pch=3, cex=0.8)
#lines(nshrinked7)
points(cifar10[[7]]$npoints.per.sample, pch=4, cex=0.8)
#lines(nshrinked9)
points(cifar10[[9]]$npoints.per.sample, pch=5, cex=0.8)
dev.off()

nhyperplane.estimation3 = cifar10[[3]]$data.dimensionality * nshrinked3^(2/(cifar10[[3]]$data.dimensionality+1))
nhyperplane.estimation5 = cifar10[[5]]$data.dimensionality * nshrinked5^(2/(cifar10[[5]]$data.dimensionality+1))
nhyperplane.estimation7 = cifar10[[7]]$data.dimensionality * nshrinked7^(2/(cifar10[[7]]$data.dimensionality+1))
nhyperplane.estimation9 = cifar10[[9]]$data.dimensionality * nshrinked9^(2/(cifar10[[9]]$data.dimensionality+1))

pdf("cifar10-nhyperplanes-estimation.pdf")
data3 = cbind(cifar10[[3]]$npoints.per.sample[,1], cifar10[[3]]$max.nhyperplanes)
data5 = cbind(cifar10[[5]]$npoints.per.sample[,1], cifar10[[5]]$max.nhyperplanes)
data7 = cbind(cifar10[[7]]$npoints.per.sample[,1], cifar10[[7]]$max.nhyperplanes)
data9 = cbind(cifar10[[9]]$npoints.per.sample[,1], cifar10[[9]]$max.nhyperplanes)
plot(data3, xlab="Sample size", ylab="Number of hyperplanes", t="p", pch=2, cex=1, ylim=c(40,266))
#lines(nhyperplane.estimation3, xlab="Sample size", ylab="Estimation of hyperplanes")
points(data5, pch=3, cex=1)
#lines(nhyperplane.estimation5)
points(data7, pch=4, cex=1)
#lines(nhyperplane.estimation7)
points(data9, pch=5, cex=1)
#lines(nhyperplane.estimation9)
dev.off()
###################################################################################
