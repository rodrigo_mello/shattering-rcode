source("../core.r")

D = cbind(rnorm(mean=0,sd=1,n=100), rnorm(mean=0,sd=1,n=100), rep(1,100))
D = rbind(D, cbind(rnorm(mean=3,sd=1,n=100), rnorm(mean=3,sd=1,n=100), rep(2,100)))

R = shrink.space(D[,1:2], data.dimensionality=2, labels=D[,3], method="complete")
id = which(R$vec == -1)

pdf("example2.pdf")
par(mfrow=c(1,3))
plot(D, pch=D[,3], xlab="Attribute 1", ylab="Attribute 2")
x_range = range(D[,1])
y_range = range(D[,2])
plot(D[id,1:2], pch=D[id,3], xlab="Attribute 1", ylab="Attribute 2", xlim=x_range, ylim=y_range)
dev.off()


