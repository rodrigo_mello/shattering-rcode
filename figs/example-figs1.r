source("../core.r")

D = cbind(rnorm(mean=0,sd=1,n=100), rnorm(mean=0,sd=1,n=100), rep(1,100))
D = rbind(D, cbind(rnorm(mean=5,sd=1,n=100), rnorm(mean=5,sd=1,n=100), rep(2,100)))

R = kmeans(D[,1:2], centers=2)


pdf("example1.pdf")
par(mfrow=c(1,2))
plot(D, pch=D[,3], xlab="Attribute 1", ylab="Attribute 2")
x_range = range(D[,1])
y_range = range(D[,2])
plot(R$centers, pch=c(1,2), xlab="Attribute 1", ylab="Attribute 2", xlim=x_range, ylim=y_range)
dev.off()


