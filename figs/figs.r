
pdf("implementation-illustration1.pdf")
ids = sample(1:150, size=30)
model = hclust(dist(iris[ids,1:4]), method="complete")
plot(model, labels=as.character(as.numeric(iris[ids,5])), xlab="Class identifiers for instances", sub="", main="")
dev.off()

pdf("implementation-illustration2.pdf")
D = cbind(rnorm(mean=0,sd=0.7,n=50), rnorm(mean=0,sd=0.7,n=50), rep(1,50))
D = rbind(D, cbind(rnorm(mean=1,sd=0.7,n=50), rnorm(mean=1,sd=0.7,n=50), rep(2,50)))
plot(D, pch=D[,3], xlab="Attribute 1", ylab="Attribute 2")
dev.off()
