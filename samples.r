require(keras)
require(arules)
require(tseriesChaos)

source("core.r")

two.gaussians <- function(params1=c(0,1), params2=c(10,1), size=250) {
	D = cbind(rnorm(mean=params1[1], sd=params1[2], n=size), 
	      rnorm(mean=params1[1], sd=params1[2], n=size))
	D = rbind(D, cbind(rnorm(mean=params2[1], sd=params2[2], n=size), 
	      rnorm(mean=params2[1], sd=params2[2], n=size)))
	classes = c(rep(1,size), rep(2,size))

	res = shrink.space(D, data.dimensionality=ncol(D), labels=classes)
	par(mfrow=c(1,2))
	plot(D, col=classes, main="Examples and their classes", xlab="Attribute 1", ylab="Attribute 2")
	plot(D, main="Counted examples in green", xlab="Attribute 1", ylab="Attribute 2")
	ids = which(res$classes == -1)
	points(D[ids,], col=3)
	locator(1)

	model = compute.shattering(dataset=D, 
				   data.dimensionality=ncol(D),
				   labels=classes,
				   samp.start=25, 
				   samp.end=size*2, 
				   by=25, shat=T)
	model$D = D

	plot(model$npoints.per.sample, main="Sample size versus Shrinked sample size",
	     	xlab="Sample size", ylab="Shrinked sample size")
	plot(model$shattering.function[,c(1,3)], main="Shattering coefficient function",
	     	xlab="Sample size", ylab="Shattering coefficient")

	return (model)
}

second.order <- function() {
	D = cbind(embedd(sin(2*pi*seq(0,9,len=1025)+rnorm(mean=0, sd=0.1, n=1025)), m=2, d=25))
	D = rbind(D, cbind(rnorm(mean=0, sd=0.3, n=1000), rnorm(mean=0, sd=0.3, n=1000)))
	classes = c(rep(1,1000), rep(2,1000))

	res = shrink.space(D, data.dimensionality=ncol(D), labels=classes)
	par(mfrow=c(1,3))
	plot(D, col=classes, main="Examples and their classes", xlab="Attribute 1", ylab="Attribute 2")
	plot(D, main="Counted examples in green", xlab="Attribute 1", ylab="Attribute 2")

	ids = which(res$classes == -1)
	ids = union(ids, which(is.na(res$classes)))
	points(D[ids,], col=3)
	locator(1)

	model = compute.shattering(dataset=D, 
				   data.dimensionality=ncol(D),
				   labels=classes, 
				   samp.start=5, 
				   samp.end=2000, 
				   by=5, shat=F)
	model$D = D

	plot(model$npoints.per.sample, main="Sample size versus Shrinked sample size",
	     	xlab="Sample size", ylab="Shrinked sample size")
	#plot(model$shattering.function[,c(1,3)], main="Shattering coefficient function",
	#     	xlab="Sample size", ylab="Shattering coefficient")

	return (model)
}

get.pixel <- function(image, k, l) {
	if (k < 1 || l < 1 || k > nrow(image) || l > ncol(image)) return (0)
	else return (image[k,l])
}

extract.vectors <- function(image, class, mask.size) {
	offset = floor(mask.size / 2)
	V = matrix(0, nrow=nrow(image)*ncol(image), ncol=mask.size^2+1)
	p = 1
	for (i in 1:nrow(image)) {
		for (j in 1:ncol(image)) {
			c = 1
			vec = rep(0, mask.size^2+1)
			# extrair pixels
			for (k in (i-offset):(i+offset)) {
				for (l in (j-offset):(j+offset)) {
					vec[c] = get.pixel(image, k, l)
					c = c + 1
				}
			}
			vec[c] = class
			V[p,] = vec
			p = p + 1
		}
	}

	return (V)
}

# MNIST
mnist <- function(sample.size=1000, mask.sizes=c(3,5,7,9), method="complete", 
		  start=28*28*10, end=28*28*sample.size, by=28*28*10, plot=F) {
	data = dataset_mnist()

	model = list()
	for (mask.size in mask.sizes) {
		cat("mask size = ", mask.size, "\n")
		D = matrix(0, nrow=sample.size*28*28, ncol=mask.size^2+1)

		# image embedding
		for (i in 1:sample.size) {
			D[((i-1)*28*28+1):(i*(28*28)),] = 
				extract.vectors(data$train$x[i,,], data$train$y[i], mask.size)
		}

		# compute the shattering coefficient function
		model[[mask.size]] = compute.shattering(dataset=D, method=method,
					   samp.start=start, 
					   samp.end=end, 
					   by=by, shat=F, partial.plot=plot)
	}
	
	return(model)
}

# CIFAR-10
cifar.10 <- function(sample.size=1000, mask.sizes=c(3,5,7,9), method="complete", 
		     start=32*32, end=40000, by=32*32, plot=F) { # 1024000
	#> dim(x$train$x)
	#[1] 50000    32    32     3
	data = dataset_cifar10()

	model = list()
	for (mask.size in mask.sizes) {
		D = matrix(0, nrow=sample.size*32*32, ncol=(3*mask.size^2)+1)

		# image embedding
		for (i in 1:sample.size) {
			red = extract.vectors(data$train$x[i,,,1], data$train$y[i], mask.size)
			green = extract.vectors(data$train$x[i,,,2], data$train$y[i], mask.size)
			blue = extract.vectors(data$train$x[i,,,3], data$train$y[i], mask.size)
			D[((i-1)*32*32+1):(i*(32*32)),] = cbind(red[,1:(mask.size^2)],
								    green[,1:(mask.size^2)],
								    blue)
		}

		# compute the shattering coefficient function
		model[[mask.size]] = compute.shattering(dataset=D, method=method,
					   samp.start=start, 
					   samp.end=end, 
					   by=by, shat=F, partial.plot=plot)
	}
	
	return(model)
}

# 2nd-order kernel
second.order.kernel <- function(method="complete", kernel=F) {

	D = cbind(rnorm(mean=0,sd=0.2,n=1000), rnorm(mean=0,sd=0.2,n=1000))
	D = rbind(D, embedd(sin(2*pi*seq(0,9,length=1025))+rnorm(mean=0,sd=0.1,n=1025), m=2, d=25))
	classes = c(rep(1,1000),rep(2,1000))

	ids = sample(1:2000)
	D = D[ids,]
	classes = classes[ids]

	#plot(D, col=classes)
	#locator(1)

	start = 5
	end=2000
	by=5

	model = NULL

	if (!kernel) {
		# compute the shattering coefficient function
		model = compute.shattering(dataset=D, 
					   data.dimensionality=ncol(D),
					   labels=classes,
					   method=method,
					   samp.start=start, 
					   samp.end=end, 
					   by=by, shat=F, 
					   partial.plot=F)
	} else {
		# 2nd-order polynomial kernel
		X = D
		D = cbind(X[,1]^2, sqrt(2)*X[,1]*X[,2], X[,2]^2)

		# compute the shattering coefficient function
		model = compute.shattering(dataset=D,
					   data.dimensionality=ncol(D),
					   labels=classes,
					   method=method,
					   samp.start=start, 
					   samp.end=end, 
					   by=by, shat=F, 
					   partial.plot=F)
	}
	
	return(model)
}

# Iris
iris.shattering <- function(method="complete") {

	D = as.matrix(iris[,1:4])
	classes = as.numeric(iris[,5])

	# Randomizing
	ids = sample(1:nrow(D))
	D = D[ids,]
	classes = classes[ids]

	plot(D, col=classes)
	locator(1)

	start = 5
	end=nrow(iris)
	by=1

	model = NULL

	model = compute.shattering(dataset=D, 
				data.dimensionality=ncol(D),
				labels=classes,
				method=method,
				samp.start=start, 
				samp.end=end, 
				by=by, shat=F, 
				partial.plot=T)
	return(model)
}
